import math


def is_prime(a: int) -> bool:
    return all(a % i != 0 for i in range(2, math.ceil(math.sqrt(a) + 1)))


def legendre_symbol(a: int, p: int) -> int:
    if not is_prime(p) or p == 2:
        raise ValueError('p should be an odd prime')
    a = a % p
    if not a:
        return 0
    if pow(a, (p - 1) // 2, p) == 1:
        return 1
    return -1
