from legendre import *
from jacobi import *


def quadratic_deduction(n: int, func) -> (list, list):
    deductions = []
    non_deductions = []
    for i in range(n):
        if math.gcd(i, n) == 1:
            result = func(i, n)
            if result == 1 or result == 0:
                deductions.append(i)
            else:
                non_deductions.append(i)
    return deductions, non_deductions


if __name__ == '__main__':
    n = 7

    print(f'Модуль = {n}')

    a = 4

    print(f'Символ Лежандра для {a}:', legendre_symbol(
        a, n
    ))

    print(f'Символ Якоби для {a}:', jacobi_symbol(
        a, n
    ))

    print(f'Квадратичные вычеты и невычеты {n} по Лежандру:', quadratic_deduction(
        n, legendre_symbol
    ))

    print(f'Квадратичные вычеты и невычеты {n} по Якоби:', quadratic_deduction(
        n, jacobi_symbol
    ))
