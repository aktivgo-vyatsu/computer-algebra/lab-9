import math


def jacobi_symbol(m: int, n: int) -> int:
    if n < 0 or not n % 2:
        raise ValueError('n should be an odd positive integer')
    if m < 0 or m > n:
        m %= n
    if not m:
        return int(n == 1)
    if n == 1 or m == 1:
        return 1
    if math.gcd(m, n) != 1:
        return 0

    j = 1
    while m != 0:
        while m % 2 == 0 and m > 0:
            m >>= 1
            if n % 8 in [3, 5]:
                j = -j
        m, n = n, m
        if m % 4 == n % 4 == 3:
            j = -j
        m %= n
    return j
